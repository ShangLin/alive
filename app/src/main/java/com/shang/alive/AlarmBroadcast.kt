package com.shang.alive

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import java.util.*

class AlarmBroadcast : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if(intent.action.equals("test")){
            var notificationUnit=NotificationUnit.getInstance(context)
            notificationUnit.push(notificationUnit.getBuilder(context,notificationUnit.getPendingIntent(context)))
        }

        if(intent.action.equals("getPendingIntent")){
            NotificationUnit.getInstance(context).closeNotification()
            var intent=Intent().setAction("com.shang.alive")
            context.startActivity(intent)
        }
    }
}
