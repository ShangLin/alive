package com.shang.alive

import android.app.AlarmManager
import android.app.AlarmManager.*
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.ads.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast
import android.view.Menu
import android.view.MenuItem


class MainActivity : AppCompatActivity() {

    lateinit var alermManager:AlarmManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this, "ca-app-pub-3596318314144695~1304764079");
        adview()

        //ELAPSED_REALTIME：在指定的延时过后，发送广播，但不唤醒设备（
        //闹钟在睡眠状态下不可用）。如果在系统休眠时闹钟触发，它将不会被传递，直到下一次设备唤醒。
        alermManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alermManager.setInexactRepeating(ELAPSED_REALTIME_WAKEUP, 1000,
            INTERVAL_HALF_HOUR, getPendingIntent())
        karma()
    }

    fun adview() {
        //記得要去XML換成 ca-app-pub-3596318314144695/3356212347
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        adView2.loadAd(adRequest)
        adView3.loadAd(adRequest)
        adView4.loadAd(adRequest)
        adView5.loadAd(adRequest)
        adView5.loadAd(adRequest)
        adView6.loadAd(adRequest)
        adView7.loadAd(adRequest)
        adView8.loadAd(adRequest)
        var adListener= object : AdListener() {
            override fun onAdClicked() {
                toast("消除業障-1")
                karma()
            }
        }
        adView.adListener =adListener
        adView2.adListener =adListener
        adView3.adListener =adListener
        adView4.adListener =adListener
        adView5.adListener =adListener
        adView6.adListener =adListener
        adView7.adListener =adListener
        adView8.adListener =adListener
    }

    fun testAdview() {
        var adRequest = AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build()
        adView.loadAd(adRequest)
        //adUnitId ca-app-pub-3596318314144695/3356212347 xml目前是測試用的 這個才是真的
        adView.adListener = object : AdListener() {
            override fun onAdClicked() {
                toast("消除業障-1")
                karma()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.my_menu, menu)
        switch(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.open -> {
                if(StoreUnit.getInstance(this).switch){
                    item.setIcon(R.drawable.ic_close)
                    alermManager.cancel(getPendingIntent())
                    toast("關閉定時通知")
                }else{
                    item.setIcon(R.drawable.ic_open)
                    alermManager.setInexactRepeating(ELAPSED_REALTIME_WAKEUP,
                        1000, INTERVAL_HALF_HOUR, getPendingIntent())
                    toast("開啟定時通知")
                }
                StoreUnit.getInstance(this).putSwitch()
                return true
            }
            R.id.refresh -> {
                refresh_picture()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        refresh_picture()
    }

    fun getPendingIntent():PendingIntent{
        var intent = Intent(this, AlarmBroadcast::class.java).setAction("test")
        val pendingIntent = PendingIntent.getBroadcast(
            this, 110
            , intent, PendingIntent.FLAG_CANCEL_CURRENT
        )
        return pendingIntent
    }

    fun refresh_picture() {
        val assetManager = assets
        val PICTURE_SIZE: Int = assetManager.list("picture").size
        Log.d("TAG", PICTURE_SIZE.toString())

        var random: Int = (Math.random() * PICTURE_SIZE).toInt() + 1
        var picture: String = "picture_$random.jpg"
        Log.d("TAG", picture)

        try {
            var io = assetManager.open("picture/$picture")
            var bitmap = BitmapFactory.decodeStream(io)
            imageView.setImageBitmap(bitmap)
        } catch (e: Exception) {
            imageView.setImageResource(R.drawable.preset)
            Log.d("TAG",e.toString())
        }
    }

    fun switch(menu: Menu){
        if(StoreUnit.getInstance(this).switch){
            menu.findItem(R.id.open).setIcon(R.drawable.ic_open)
        }else{
            menu.findItem(R.id.open).setIcon(R.drawable.ic_close)
        }
    }

    fun karma(){
        StoreUnit.getInstance(this).putKarma()
        karma.setText("消業值:"+StoreUnit.getInstance(this).karma)
    }
}
