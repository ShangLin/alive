package com.shang.alive;

import android.content.Context;
import android.content.SharedPreferences;

class StoreUnit {
    private static StoreUnit ourInstance = null;

    static StoreUnit getInstance(Context context) {

        if (ourInstance == null) {
            ourInstance = new StoreUnit(context);
        }

        return ourInstance;
    }

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    private static final String Karma = "KARMA";
    private static final String Switch = "SWITCH";

    private StoreUnit(Context context) {
        sharedPreferences = context.getSharedPreferences("ALIVE", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    protected void putKarma() {
        int karma = sharedPreferences.getInt(Karma, 0);

        if (karma == 0) {
            editor.putInt(Karma, (int) (Math.random() * 999) + 1).commit();
        } else {
            editor.putInt(Karma, karma - 1).commit();
        }
    }

    protected int getKarma() {
        return sharedPreferences.getInt(Karma, 0);
    }

    protected void putSwitch() {
        boolean sw = sharedPreferences.getBoolean(Switch, true);
        if (sw) {
            editor.putBoolean(Switch, false).commit();
        } else {
            editor.putBoolean(Switch, true).commit();
        }
    }

    protected Boolean getSwitch() {
        return sharedPreferences.getBoolean(Switch, true);
    }
}
