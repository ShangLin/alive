package com.shang.alive

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build

class NotificationUnit {

    companion object {
        val Notification_ID = 906
        val channel_ID = "com.shang.alive"
        lateinit var channel: NotificationChannel
        lateinit var manager: NotificationManager
        var myNotificationUnit: NotificationUnit? = null

        //單例化
        fun getInstance(context: Context): NotificationUnit {
            if (myNotificationUnit == null) {
                myNotificationUnit = NotificationUnit()
                manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    channel = NotificationChannel(
                        channel_ID,
                        context.getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_LOW
                    )
                    channel.enableLights(false)
                    channel.enableVibration(false)
                    manager.createNotificationChannel(channel)
                }

            }
            return myNotificationUnit as NotificationUnit
        }
    }

    fun getBuilder(context: Context,pendingIntent: PendingIntent):Notification{
        lateinit var builder: Notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            builder = Notification.Builder(context, channel_ID).apply {
                this.setSmallIcon(R.drawable.ic_launcher_foreground)
                this.setLargeIcon(
                    android.graphics.BitmapFactory.decodeResource(
                        context.resources,
                        R.drawable.ic_launcher_foreground
                    )
                )
                this.setContentTitle("每日廢話")
                this.setContentText(Useless.getUseless())
                //this.setCustomContentView(getRemoteViews(context, name, picture))
                this.setChannelId(channel_ID)
                this.setContentIntent(pendingIntent)
            }.build()
        }else{
            builder = Notification.Builder(context).apply {
                //this.setContent(getRemoteViews(context, name, picture))
                this.setSmallIcon(R.drawable.ic_launcher_foreground)
                this.setLargeIcon(
                    android.graphics.BitmapFactory.decodeResource(
                        context.resources,
                        R.drawable.ic_launcher_foreground
                    )
                )

                this.setContentTitle("每日廢話")
                this.setContentText(Useless.getUseless())
                this.setContentIntent(pendingIntent)
            }.build()
        }
        return builder
    }

    fun push(builder:Notification) {
        manager.notify(Notification_ID, builder)
    }

    fun getPendingIntent(context: Context):PendingIntent{
        var intent= Intent(context,AlarmBroadcast::class.java).apply {
            this.action="getPendingIntent"
        }
        var pendingIntent=PendingIntent
            .getBroadcast(context,100,intent,PendingIntent.FLAG_CANCEL_CURRENT)


        return pendingIntent
    }

    fun closeNotification(){
        manager.cancel(Notification_ID)
    }


}